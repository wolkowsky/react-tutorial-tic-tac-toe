import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

export const SIZE = 3;

class App extends Component {
  render() {
    return (            
        <Game />      
    );
  }
}

export default App;

function Square(props) {
  return (
    <button className="square" onClick={props.onClick}>
      {props.value}
    </button>
  );
}

class Board extends Component {
    renderSquare(i) {    
    return (
      <Square 
        value={this.props.squares[i]} 
        onClick={() => this.props.onClick(i)}/>
    );
  }

  render() {     
    var rows = [];
    let cols = [];
    for (var r = 0; r < SIZE; r++) {
      for (var i = r * SIZE; i < r*SIZE + SIZE; i++) {
        cols.push(this.renderSquare(i));
      }
      rows.push(<div key={r} className="board-row">{cols}</div>);
      cols = [];
    }

    return <div>{rows}</div>;    
  }
}

class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      history: [{
        squares: Array(9).fill(null),
        location: Array(0),
      }],
      stepNumber: 0,   
      xIsNext: true,
    };
  }

  handleClick(i) {
    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();
    const location = current.location.slice();

    if (squares[i] || calculateWinner(squares)) {
      return;
    }

    squares[i] = this.state.xIsNext ? "X" : "O";
    location.push(i); 

    this.setState({
      history: history.concat([{
        squares: squares,
        location: location,
      }]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }

  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: (step % 2) === 0,
    });
  }

  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    const stepNumber = this.state.stepNumber;
    const winner = calculateWinner(current.squares);

    const className = 'active';

    const moves = history.map((step, move) => {
      const desc = move ? 
        `Go to move #${move} ${getLocation(step.location[move - 1])}`:
        `Go to game start`;

      return (
        <li key={move}>
          <button className={stepNumber === move ? className : null} onClick={() => {this.jumpTo(move)}}>{desc}</button>
        </li>
      )
    });

    let status;
    if (winner) {
      status = 'Winner: ' + winner;
    } else {
      status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
    }

    return (
      <div className="game">
        <div className="game-board">
          <Board 
            squares={current.squares}
            onClick={i => this.handleClick(i)}/>
        </div>
        <div className="game-info">
          <div>{status}</div>
          <ol>{moves}</ol>
        </div>
      </div>
    );
  }
}

function calculateWinner(squares) {
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
  ];

  for(let i=0; i < lines.length; i++) {
    const [a, b, c] = lines[i];

    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }

  return null;
}

function getLocation(squareNumber) {  
  let location = { x: 1, y: 1}

  location.y = Math.floor(squareNumber / SIZE) + 1;
  location.x = (squareNumber % SIZE) + 1;

  return `(${location.x},${location.y})`;
} 